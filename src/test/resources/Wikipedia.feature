Feature:
  Search an article about Test Automation on Wikipedia
  
  Scenario: Search article
    Given Navigation to URL "http://en.wikipedia.org" on Chrome
    When Search by "Test automation"
    Then Article related to "Test automation" is shown

