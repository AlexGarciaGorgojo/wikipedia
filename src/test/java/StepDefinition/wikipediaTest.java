package StepDefinition;

import commons.Connection;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.openqa.selenium.WebDriver;
import static org.junit.Assert.assertTrue;

import wikipedia.MainPage;
import wikipedia.ResultsPage;

public class wikipediaTest {
    public Connection connection;
    WebDriver driver;

    @After
    public void closeBrowser(){
        connection.closeConnection();
    }

    @Given("^Navigation to URL \"([^\"]+)\" on (Firefox|Chrome|InternetExplorer)$")
    public void openBrowserAndNagivateToURL(String pUrl, String pBrowser){
        //Start the parametrized browser navigating to the url.
        connection  = new Connection(pBrowser, pUrl);

        //Store the driver in order to reuse it.
        driver = connection.getDriver();
    }

    @When("^Search by \"([^\"]+)\"$")
    public void searchOnWebBy(String pValueToSearch){
        //Search the word on the search bar
        MainPage.searchTopic(driver, pValueToSearch);
        //Click on the search button
        MainPage.clickSearchButton(driver);
    }

    @Then("^Article related to \"([^\"]+)\" is shown$")
    public void resultsRelatedTo(String pMatchingValue){
        //Get the article from the shown page.
        String articleShown = ResultsPage.getArticleHead(driver);
        //Assert the result.
        assertTrue("Article shown is about " + pMatchingValue, pMatchingValue.equals(articleShown));
    }



}
