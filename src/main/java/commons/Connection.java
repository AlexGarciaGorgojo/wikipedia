package commons;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import wikipedia.ResultsPage;


public class Connection{

    final static Logger logger = Logger.getLogger(ResultsPage.class);
    public WebDriver driver;

    public Connection(String pBrowserType, String pURL ){

        String s = pBrowserType.toUpperCase();
        if (s.equals("FIREFOX")) {
            System.setProperty("webdriver.gecko.driver", "./src/main/resources/drivers/geckodriver.exe");
            driver = new FirefoxDriver();
            logger.info("Firefox instance initialized.\n");

        } else if (s.equals("CHROME")) {
            System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
            driver = new ChromeDriver();
            logger.info("Chrome instance initialized.\n");

        } else if (s.equals("INTERNETEXPLORER")) {
            System.setProperty("webdriver.ie.driver", "./src/main/resources/drivers/IEDriverServer.exe");
            driver = new InternetExplorerDriver();
            logger.info("Internet Explorer instance initialized.\n");

        } else {
            driver = null;
            logger.info("The driver '" + pBrowserType + "' is not valid. Please check if the name is correct or implement it.\n");
            System.exit(0);
        }

        driver.get(pURL);
    }


    public WebDriver getDriver()
    {
        return driver;
    }

    public void closeConnection(){
        logger.info("Closing browser...\n");
        driver.close();
    }
}
