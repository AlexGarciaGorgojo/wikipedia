package wikipedia;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultsPage {
    final static Logger logger = Logger.getLogger(ResultsPage.class);

    static By articleHead = By.id("firstHeading");

    public static String getArticleHead(WebDriver driver){
        logger.info("Get article head value.\n");
        return driver.findElement(articleHead).getText();
    }
}
