package wikipedia;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by KANTET5 on 14/03/2019.
 */
public class MainPage {
    final static Logger logger = Logger.getLogger(MainPage.class);

    static By searchTextbox = By.xpath("//*[@id='searchInput']");
    static By searchButton = By.xpath("//*[@id='searchButton']");

    public static void searchTopic(WebDriver pDriver, String pTopic) {
        logger.info("Search '" + pTopic + "'.\n");
        pDriver.findElement(searchTextbox).sendKeys(pTopic);
    }

    public static void clickSearchButton(WebDriver pDriver){
        logger.info("Click search button.\n");
        pDriver.findElement(searchButton).click();
    }


}
